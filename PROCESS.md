# `release-cli` Processes

## Versioning

> `release-cli` follows [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The `X-Y-stable` branches and `master` should never have their history
rewritten. Tags should never be deleted.
